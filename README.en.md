# python3_grab_subtitle

#### Description
Python 3 grab subtitle for the films in your NAS via api 

#### Instructions

1.  git clone git@itgeeker.net:itgeeker/python3_grab_subtitle.git
2.  cd /python3_grab_subtitle
3.  python3 grab_subtitle_assert_net.py

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### ITGeeker Feature

1.  ITGeeker Official blog [www.itgeeker.net](https://www.itgeeker.net)
2.  Please visit [https://www.itgeeker.net/python3_grab_subtitle/](https://www.itgeeker.net/python3_grab_subtitle/) to read more details.
