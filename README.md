# python3_grab_subtitle

#### 介绍
Python 3通过API给NAS里的电源获取电影字幕。

#### 软件架构
    硬件环境：Synology 418play DSM6.2.3群辉系统
    Python版本： Python3.8.2
    Python模块：
        import json
        #python3.4版本之后才支持pathlib
        from pathlib import Path
        from sys import platform
        import requests
        import urllib.request


#### 使用说明

1.  git clone git@itgeeker.net:itgeeker/python3_grab_subtitle.git
2.  cd /python3_grab_subtitle
3.  python3 grab_subtitle_assert_net.py


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  ITGeeker技术奇客官方博客 [www.itgeeker.net](https://www.itgeeker.net)
2.  你可以 [https://www.itgeeker.net/python3_grab_subtitle/](https://www.itgeeker.net/python3_grab_subtitle/) 这个地址来了解项目开发的详细过程

